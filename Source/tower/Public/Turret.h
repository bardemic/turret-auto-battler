// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Turret.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTurretRangeChange, float, Range);

UCLASS()
class TOWER_API ATurret : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Target1;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Target2;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* FollowTarget;

	UPROPERTY()
		FTimerHandle ChangeTargetTimerHandle;

	UPROPERTY()
		FTimerHandle BeamLengthTimerHandle;

	// Rotation
	FRotator LookRotation;
	FRotator TargetRotation;
	FRotator RotationDelta;
	int TimerCount = 0;

	UPROPERTY(EditAnywhere)
		float ChangeTargetDelay = 5.f;

	UPROPERTY(EditAnywhere)
		float RotationRateMultiplier = .2f;

	UFUNCTION()
		void UpdateLookAtTarget(float DeltaTime);

	UFUNCTION()
		void ChangeBeamTarget();

	UFUNCTION(BlueprintCallable)
		void SetBeamLength(float Length);

	UFUNCTION()
		void TraceBeam();

	UFUNCTION()
		void CheckEnemy(AActor* HitActor);

	UFUNCTION()
		void FollowEnemy(float DeltaTime);
	
public:	
	// Sets default values for this actor's properties
	ATurret();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetRange(int Range);
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BeamLength = 1000.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int RangeRadius = 30;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		USkeletalMeshComponent* TurretMesh;
	UPROPERTY(BlueprintReadWrite)
		AActor* Enemy;
	UPROPERTY(BlueprintReadWrite)
		TArray<AActor*> EnemiesInRange;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Beam;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* RangeCollider;
	//Targeting functions
	UFUNCTION(BlueprintCallable)
		void TargetHighestHealth();
	UFUNCTION(BlueprintCallable)
		void TargetLeastHealth();
	UFUNCTION(BlueprintCallable)
		void TargetClosest();
	UFUNCTION(BlueprintCallable)
		void TargetFurthest();
	UPROPERTY(BlueprintAssignable, Category="EventDispatchers")
		FOnTurretRangeChange RangeChange;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
