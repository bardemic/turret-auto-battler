﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once



#include "CoreMinimal.h"
#include "DamageTypesEnum.h"
#include "GameFramework/Character.h"
#include "BaseEnemy.generated.h"

UCLASS()
class TOWER_API ABaseEnemy : public ACharacter
{
	GENERATED_BODY()
private:
	struct FDotsStruct
	{
		float StartTime;
		float EndTime;
		float LastTick;
		int Damage;
	};
	TMap<EDamageTypes, FDotsStruct> ActiveDots;

public:
	// Sets default values for this character's properties
	ABaseEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
		void AddDotEffect(int Damage, float Duration, EDamageTypes Type);
	UFUNCTION(BlueprintCallable)
		void CheckDotEffects();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void TakeAmountDamage(int Damage);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
