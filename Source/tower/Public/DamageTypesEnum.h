// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedEnum.h"
#include "DamageTypesEnum.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EDamageTypes : uint8 {
	Normal UMETA(DisplayName="Normal"),
	Earth UMETA(DisplayName="Earth"),
	Death UMETA(DisplayName="Death"),
	Water UMETA(DisplayName="Water"),
	Fire UMETA(DisplayName="Fire"),
	Ice	UMETA(DisplayName="Ice"),
	Crystal	UMETA(DisplayName="Crystal"),
	Electric UMETA(DisplayName="Electric"),
};
