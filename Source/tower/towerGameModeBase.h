// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "towerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_API AtowerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
