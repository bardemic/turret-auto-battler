// Fill out your copyright notice in the Description page of Project Settings.


#include "Turret.h"
#include "EnemyInterface.h"
#include "TurretAnimInterface.h"
#include "VectorTypes.h"
#include "Kismet/KismetMathLibrary.h"

void ATurret::UpdateLookAtTarget(float DeltaTime)
{
	if (LookRotation.Equals(TargetRotation, 1.f))
	{
		return;
	}
	
	LookRotation += RotationDelta * RotationRateMultiplier * DeltaTime;

	if (TurretMesh->GetAnimInstance()->Implements<UTurretAnimInterface>())
	{
		ITurretAnimInterface::Execute_UpdateLookRotation(TurretMesh->GetAnimInstance(), LookRotation);
	}
}

void ATurret::ChangeBeamTarget()
{
	TimerCount++;

	if (TimerCount%2 == 0)
	{
		FollowTarget->SetWorldLocation(Target1->GetComponentLocation());
	}
	else
	{
		FollowTarget->SetWorldLocation(Target2->GetComponentLocation());
	}

	FVector Start = TurretMesh->GetSocketLocation("BeamSocket");
	FVector End = FollowTarget->GetSocketLocation("FollowTarget");
	TargetRotation = UKismetMathLibrary::FindLookAtRotation(Start, End);

	RotationDelta = TargetRotation - LookRotation;
	RotationDelta.Normalize();
}

void ATurret::SetBeamLength(float Length)
{
	Beam->SetRelativeScale3D(FVector(Length/100, Beam->GetRelativeScale3D().Y, Beam->GetRelativeScale3D().Z));
	Beam->SetRelativeLocation(FVector(Length/-2, 0, 0));
}

void ATurret::TraceBeam()
{
	FHitResult HitResult;
	FVector Start = TurretMesh->GetSocketLocation("BeamSocket");
	FVector End = Start + Beam->GetForwardVector() * BeamLength;

	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(this);
	bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Camera, CollisionQueryParams);
	if (bHit)
	{
		SetBeamLength(HitResult.Distance);
	}
	else
	{
		SetBeamLength(BeamLength);
	}
}

void ATurret::CheckEnemy(AActor* HitActor)
{
	if (HitActor->Implements<UEnemyInterface>())
	{
		if (!EnemiesInRange.Contains(HitActor))
		{
			Enemy = nullptr;
		}
		else
		{
			Enemy = HitActor;
		}
	}
	else
	{
		Enemy = nullptr;
	}
}

void ATurret::FollowEnemy(float DeltaTime)
{
	FVector Start = TurretMesh->GetSocketLocation("BeamSocket");
	FVector End = Enemy->GetActorLocation();

	FRotator RotationToEnemy = UKismetMathLibrary::FindLookAtRotation(Start, End);
	LookRotation = FMath::RInterpTo(LookRotation, RotationToEnemy, DeltaTime, 100);
	
	if (TurretMesh->GetAnimInstance()->Implements<UTurretAnimInterface>())
	{
	 	ITurretAnimInterface::Execute_UpdateLookRotation(TurretMesh->GetAnimInstance(), LookRotation);
	}
}

void ATurret::SetRange_Implementation(const int Range)
{
	RangeCollider->SetSphereRadius(Range*10.f);
	RangeChange.Broadcast(Range);
}

// Sets default values
ATurret::ATurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	TurretMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TurretMesh"));
	TurretMesh->SetupAttachment(Root);
	TurretMesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);
	TurretMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Block);

	Beam = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Beam"));
	Beam->SetupAttachment(TurretMesh, TEXT("BeamSocket"));

	Target1 = CreateDefaultSubobject<USceneComponent>(TEXT("Target1"));
	Target1->SetupAttachment(Root);

	Target2 = CreateDefaultSubobject<USceneComponent>(TEXT("Target2"));
	Target2->SetupAttachment(Root);

	FollowTarget = CreateDefaultSubobject<USceneComponent>(TEXT("FollowTarget"));
	FollowTarget->SetupAttachment(Root);

	RangeCollider = CreateDefaultSubobject<USphereComponent>(TEXT("RangeCollider"));
	RangeCollider->SetupAttachment(Root);
	RangeCollider->SetSphereRadius(RangeRadius*10.f);

}

// Called when the game starts or when spawned
void ATurret::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(ChangeTargetTimerHandle, this, &ATurret::ChangeBeamTarget, ChangeTargetDelay, true, 1.f);
	GetWorldTimerManager().SetTimer(BeamLengthTimerHandle, this, &ATurret::TraceBeam, .1f, true, .1f);
	
}

void ATurret::TargetHighestHealth()
{
	if(EnemiesInRange.IsEmpty())
	{
		return;
	}
	AActor* Highest = EnemiesInRange[0];
		for (AActor* EnemyTemp : EnemiesInRange)
		{
			if(!IEnemyInterface::Execute_IsShielded(EnemyTemp)&&IEnemyInterface::Execute_GetHealth(Highest)<IEnemyInterface::Execute_GetHealth(EnemyTemp))
			{
				Highest = EnemyTemp;
			}
		}
	Enemy = Highest;
}

void ATurret::TargetLeastHealth()
{
	if(EnemiesInRange.IsEmpty())
	{
		return;
	}
	AActor* Least = EnemiesInRange[0];
	for (AActor* EnemyTemp : EnemiesInRange)
	{
		if(!IEnemyInterface::Execute_IsShielded(EnemyTemp)&&IEnemyInterface::Execute_GetHealth(Least)>IEnemyInterface::Execute_GetHealth(EnemyTemp))
		{
			Least = EnemyTemp;
		}
	}
	Enemy = Least;
}

void ATurret::TargetClosest()
{
	if(EnemiesInRange.IsEmpty())
	{
		return;
	}
	AActor* Closest = EnemiesInRange[0];
	for (AActor* EnemyTemp : EnemiesInRange)
	{
		if(!IEnemyInterface::Execute_IsShielded(EnemyTemp)&&IEnemyInterface::Execute_GetTraveled(Closest)<IEnemyInterface::Execute_GetTraveled(EnemyTemp))
		{
			Closest = EnemyTemp;
		}
	}
	Enemy = Closest;
}

void ATurret::TargetFurthest()
{
	if(EnemiesInRange.IsEmpty())
	{
		return;
	}
	AActor* Furthest = EnemiesInRange[0];
	for (AActor* EnemyTemp : EnemiesInRange)
	{
		if(!IEnemyInterface::Execute_IsShielded(EnemyTemp)&&IEnemyInterface::Execute_GetTraveled(Furthest)>IEnemyInterface::Execute_GetTraveled(EnemyTemp))
		{
			Furthest = EnemyTemp;
		}
	}
	Enemy = Furthest;
}

// Called every frame
void ATurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Enemy)
	{
		FollowEnemy(DeltaTime);
	}
	else
	{
		UpdateLookAtTarget(DeltaTime);	
	}
}

