﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseEnemy.h"

#include "Kismet/GameplayStatics.h"


// Sets default values
ABaseEnemy::ABaseEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABaseEnemy::AddDotEffect(const int Damage, const float Duration, const EDamageTypes Type)
{
	const float Current = GetWorld()->GetTimeSeconds();
	const float End = Current+Duration;
	if(ActiveDots.Contains(Type))
	{
		if (ActiveDots[Type].Damage<Damage)
		{
			ActiveDots[Type].Damage = Damage;
		}
		if (ActiveDots[Type].EndTime<End)
		{
			ActiveDots[Type].EndTime = End;
		}
	}
	else
	{
		ActiveDots.Add(Type, FDotsStruct{Current, End,Current, Damage});
	}
}

void ABaseEnemy::CheckDotEffects()
{
	const float Curr = GetWorld()->GetTimeSeconds();
	TArray<EDamageTypes> Finished;
	for(auto& [type, Dot]: ActiveDots)
	{
		if(Dot.LastTick+1<=Curr)
		{
			TakeAmountDamage(Dot.Damage);
			ActiveDots[type].LastTick = Curr;
		}
		if(Dot.EndTime<=Curr)
		{
			Finished.Add(type);
		}
	}
	for(const EDamageTypes Type : Finished)
	{
		ActiveDots.Remove(Type);
	}
}

void ABaseEnemy::TakeAmountDamage_Implementation(int Damage)
{
	//Implemented in BP_BaseEnemy
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Error didn't call correct function"));
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

